#!/bin/sh

# https://github.com/freescout-helpdesk/freescout/wiki/Installation-Guide#2-installing-package-dependencies

sudo apt install nginx php8.1 php8.1-mysql php8.1-fpm php8.1-mbstring php8.1-xml php8.1-imap php8.1-zip php8.1-gd php8.1-curl php8.1-intl mariadb-server mariadb-client git

sudo systemctl start mariadb

sudo mysql -u root

```sql
CREATE DATABASE `freescout` CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON `freescout`.* TO `freescout`@`localhost` IDENTIFIED BY "freescout";
```

```
sudo mkdir -p /var/www/html
sudo chown www-data:www-data /var/www/html
cd /var/www/html
rm -rf *
sudo git clone https://github.com/freescout-helpdesk/freescout .
sudo chown -R www-data:www-data /var/www/html
sudo useradd --groups www-data --no-create-home --shell /usr/sbin/nologin freescout
sudo find /var/www/html -type f -exec chmod 664 {} \;
sudo find /var/www/html -type d -exec chmod 755 {} \;
```

```
# configure nginx
cp freescout.nginx /etc/nginx/sites-enabled/default
sudo nginx -t
sudo systemctl start nginx.service
sudo systemctl start php8.1-fpm.service
```

 .... configure via web installer ....

add this to cronjobs

```
* * * * * php /var/www/html/artisan schedule:run >> /dev/null 2>&1
```

```sh
sudo  php /var/www/html/artisan schedule:run
Running scheduled command: '/usr/bin/php8.1' 'artisan' freescout:fetch-monitor > '/dev/null' 2>&1
Running scheduled command: '/usr/bin/php8.1' 'artisan' freescout:check-conv-viewers > '/dev/null' 2>&1
Running scheduled command: '/usr/bin/php8.1' 'artisan' freescout:fetch-emails --identifier=39411ef6fcb56cbe591d58155b391540 > '/var/www/html/storage/logs/fetch-emails.log' 2>&1
Running scheduled command: '/usr/bin/php8.1' 'artisan' queue:work --queue='emails,default,41ee19c853ee56b2058b630aef7d479b' --sleep=5 --tries=1 --timeout=1800 > '/var/www/html/storage/logs/queue-jobs.log' 2>&1
📦[michal@tickets html]$ cat /var/www/html/storage/logs/fetch-emails.log
[2024-03-16 14:14:08] Fetching UNREAD emails for the last 3 days.
[2024-03-16 14:14:08] Fetching finished
📦[michal@tickets html]$ cat /var/www/html/storage/logs/queue-jobs.log
[2024-03-16 14:14:08] Processing: App\Jobs\RestartQueueWorker
```

install artisan timer
TODO: run as freescout user?
TODO: how often it should run?
```
sudo cp freescout-*.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable freescout-artisan.timer
```
