# ticketing systems

## Comparsion

## Freescout

| Name      | License | Stars | Forks | Issues | Stack | Cloud | open-core |
|-----------|---------|-------|-------|--------|-------|-------|-----------|
| freescout | AGPLv3 | 2.5K | 441 | 24 | Nginx/Apache + Laravel + PHP + MysSQL/Postgres | $3-13EUR | No (paid AGPL modules) |

 * old school PHP + cronjobs application
 * NO freemium model, but paid AGPL modules
 * extensive documentation
 * not recommended with PHP 8.1
 * not (yet) compatible with openSUSE Tumbleweed because of php-imap dependency
 * I was able to install
 * Manual installation https://github.com/freescout-helpdesk/freescout/wiki/Installation-Guide#82-manual-installation
 * Needs a few cronjobs https://github.com/freescout-helpdesk/freescout/wiki/Installation-Guide#9-configuring-cron-jobs
 * seems to be OK with a security https://github.com/freescout-helpdesk/freescout/wiki/FreeScout-Security

## osTicket

| Name      | License | Stars | Forks | Issues | Stack | Cloud | open-core |
|-----------|---------|-------|-------|--------|-------|-------|-----------|
| osTicket | GPLv2 | 3K | 1.6K | 808 | Apache + PHP + MySQL | from $12/person/month | no |

 * no mobile app support
 * as great UI/UX as Bugzilla has
 * old school PHP application

## Zammand

| Name      | License | Stars | Forks | Issues | Stack | Cloud | open-core |
|-----------|---------|-------|-------|--------|-------|-------|-----------|
| zammad | AGPLv3 | 4K | 707 | 484 | Ruby + Node.JS | from 5EUR to 24 | Yes |

 * harder to self-host
 * needs postgres/redis/elastic search
 * has rpm packages for SLES15
 * does not handle the postgres/redis deps
 * install guide does not tell one how to configure the postgres
 * I personally don't know Rails well

## Faveo

| Name      | License | Stars | Forks | Issues | Stack | Cloud | open-core |
|-----------|---------|-------|-------|--------|-------|-------|-----------|
| osTicket | OSL-3.0 | 1.1K | 540 | 138 | Apache + PHP + MySQL | from $18/person/month | yes (community+pro+enterprise) |

 * freemium model
 * has a mobile app
 * haven't tried to deploy
 * very little development in 2019, 2020, 2021 and 2022

## Lists

https://colorlib.com/wp/open-source-ticketing-system/
https://en.wikipedia.org/wiki/Comparison_of_help_desk_issue_tracking_software

 * [UVdesk](https://github.com/uvdesk)
 * [FreeScout](https://freescout.net/)
 * [Faveo](https://www.faveohelpdesk.com/)
 * [OpenSupports](https://github.com/opensupports/opensupports)
 * [Freshdesk](https://www.freshworks.com/freshdesk/)
 * [osTicket](https://osticket.com/)
 * [DiamanteDesk](https://diamantedesk.com/)
 * [Zammad](https://zammad.org/)
 * [Support Genix](https://supportgenix.com/)
 * [HESK](https://www.hesk.com/)
 * [Spiceworks](https://www.spiceworks.com/)

https://isotropic.co/best-open-source-ticketing-systems/

 * [osTicket](https://osticket.com/)
 * [Redmine](https://www.redmine.org/plugins/service_desk)
 * [Zammad](https://zammad.org/)
 * [Best practical](https://bestpractical.com/)
 * [FreeScout](https://freescout.net/)
 * [Faveo](https://www.faveohelpdesk.com/)
 * [Hubspot](https://www.hubspot.com/products/service/help-desk)

https://www.freshbooks.com/hub/productivity/open-source-ticketing-system

 * [JitBit](https://www.jitbit.com/helpdesk/)
 * [UVdesk](https://github.com/uvdesk)
 * [osTicket](https://osticket.com/)
 * [Hesk](https://www.hesk.com/)
 * [Helpy](https://helpy.io/)

